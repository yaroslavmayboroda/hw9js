'use strict' ;

function displayList(array, parent = document.body) {
    const ul = document.createElement('ul');
    array.forEach(item => {
        const li =document.createElement('li');
        li.appendChild(document.createTextNode(item));
        ul.appendChild(li);
    });
    parent.appendChild(ul);
}

const myArrat = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const anotherArray = ["1", "2", "3", "sea", "user", 23];
displayList(anotherArray);


// 1. const newElement = document.createElement('mytag');
// newElement.textContent = 'мой тег!';
// const parentElement = document.body;
// parentElement.appendChild(newElement);

// 2. Перший параметр цієї функції вказує на те, куди саме буде вставлений HTML-код
//     beforebegin
//     afterbegin
//     beforeend
//     afterend

// 3. За допомогою метода remove()

